package de.summerfeeling.screen2imgur.publisher;

public enum Result {
	
	UPLOAD_DONE,
	UPLOAD_ERROR,
	FILE_NOT_FOUND,
	FILE_NOT_ACCESSIBLE
	
}
