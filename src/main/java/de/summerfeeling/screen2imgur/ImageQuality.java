package de.summerfeeling.screen2imgur;

public enum ImageQuality {
	
	HD(1280, 720),
	FULL_HD(1920, 1080),
	ORIGINAL(0, 0);
	
	private int width;
	private int height;
	
	ImageQuality(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
}
