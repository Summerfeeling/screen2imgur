package de.summerfeeling.screen2imgur;

import de.summerfeeling.screen2imgur.publisher.Publisher;
import de.summerfeeling.screen2imgur.publisher.Result;
import net.labymod.api.LabyModAddon;
import net.labymod.api.events.MessageSendEvent;
import net.labymod.core.LabyModCore;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement.IconData;
import net.labymod.settings.elements.HeaderElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Material;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.List;

public class Screen2Imgur extends LabyModAddon {
	
	private static Screen2Imgur instance;
	
	private boolean copyToClipboard = false;
	private boolean directUpload = false;
	private boolean enabled = true;
	
	public void onEnable() {
		Screen2Imgur.instance = this;
		
		// Registers
		getApi().getEventManager().register((MessageSendEvent) message -> {
			if (message.startsWith("-uploadscreenshot")) {
				EntityPlayerSP player = LabyModCore.getMinecraft().getPlayer();
				
				try {
					String filePath = message.substring(message.indexOf("\"") + 1, message.lastIndexOf("\""));
					
					Publisher.publishScreenshot(new File(filePath), (result, detail) -> {
						if (result == Result.UPLOAD_DONE) {
							player.addChatMessage(new ChatComponentText("§aUpload finished: " + detail).setChatStyle(new ChatStyle().setChatClickEvent(new ClickEvent(Action.OPEN_URL, detail))));
							if (copyToClipboard) Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(detail), new StringSelection(detail));
						} else {
							player.addChatMessage(new ChatComponentText("§cUpload failed: §f" + detail));
						}
					});
				} catch (Exception e) {
					player.addChatMessage(new ChatComponentText("§cSomething went wrong! Did you execute the upload command yourself? (You must not!)"));
				}
				
				return true;
			}
			
			return false;
		});
	}
	
	public void onDisable() {
	
	}
	
	public void loadConfig() {
		this.enabled = !getConfig().has("enabled") || getConfig().get("enabled").getAsBoolean();
		this.copyToClipboard = getConfig().has("copyToClipboard") && getConfig().get("copyToClipboard").getAsBoolean();
		this.directUpload = getConfig().has("directUpload") && getConfig().get("directUpload").getAsBoolean();
	}
	
	protected void fillSettings(List<SettingsElement> list) {
		list.clear();
		
		list.add(new HeaderElement("§6Screen2Imgur §7» §bSummerfeeling"));
		list.add(new BooleanElement("Enabled", this, new IconData(Material.LEVER), "enabled", true));
		list.add(new BooleanElement("Automatic copy to clipboard", this, new IconData(Material.BOOK_AND_QUILL), "copyToClipboard", false).bindDescription("Should Screen2Imgur put the link automatically into your clipboard?"));
		list.add(new BooleanElement("Automatic upload", this, new IconData(Material.FIREWORK), "directUpload", false).bindDescription("Should screenshots automatically be uploaded to imgur?"));
		
		list.add(new HeaderElement(" "));
		list.add(new HeaderElement("§oYour upload is limited to 500 screenshots per day due to imgur's rate limit"));
		list.add(new HeaderElement("§oSo be careful using automatic upload!"));
	}
	
	public static Screen2Imgur getInstance() {
		return instance;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public boolean isDirectUpload() {
		return directUpload;
	}
	
	public boolean isCopyToClipboard() {
		return copyToClipboard;
	}
}
