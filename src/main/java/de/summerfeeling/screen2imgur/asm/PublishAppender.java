package de.summerfeeling.screen2imgur.asm;

import de.summerfeeling.screen2imgur.Screen2Imgur;
import de.summerfeeling.screen2imgur.publisher.Publisher;
import de.summerfeeling.screen2imgur.publisher.Result;
import net.labymod.core.LabyModCore;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.event.HoverEvent.Action;
import net.minecraft.util.*;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;

public class PublishAppender {
	
	public static IChatComponent appendPublisher(IChatComponent component) {
		if (!Screen2Imgur.getInstance().isEnabled()) return component;
		
		ChatComponentTranslation translation = (ChatComponentTranslation) component;
		ChatComponentText displayMessage = (ChatComponentText) translation.getFormatArgs()[0];
		
		String fileName = displayMessage.getChatStyle().getChatClickEvent().getValue();
		String displayName = displayMessage.getUnformattedTextForChat();
		
		if (Screen2Imgur.getInstance().isDirectUpload()) {
			Publisher.publishScreenshot(new File(fileName), (result, detail) -> {
				EntityPlayerSP player = LabyModCore.getMinecraft().getPlayer();
				
				if (result == Result.UPLOAD_DONE) {
					player.addChatMessage(new ChatComponentText("§aUpload finished: " + detail).setChatStyle(new ChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, detail))));
					if (Screen2Imgur.getInstance().isCopyToClipboard()) Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(detail), new StringSelection(detail));
				} else {
					player.addChatMessage(new ChatComponentText("§cUpload failed: §f" + detail));
				}
			});
			
			return new ChatComponentText("Saved screenshot as ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD))
					.appendSibling(new ChatComponentText(displayName).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD).setUnderlined(true).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Open screenshot"))).setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, fileName))));
		}
		
		return new ChatComponentText("Saved screenshot as ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD))
				.appendSibling(new ChatComponentText(displayName).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD).setUnderlined(true).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Open screenshot"))).setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, fileName))))
				.appendSibling(new ChatComponentText(" [UPLOAD]").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.YELLOW).setBold(true).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Upload to imgur"))).setChatClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "-uploadscreenshot \"" + fileName + "\""))));
		
	}
	
}
