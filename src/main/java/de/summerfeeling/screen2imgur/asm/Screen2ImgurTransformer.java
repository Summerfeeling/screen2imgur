package de.summerfeeling.screen2imgur.asm;

import de.summerfeeling.screen2imgur.asm.implementation.MC112Implementation;
import de.summerfeeling.screen2imgur.asm.implementation.MC18Implementation;
import de.summerfeeling.screen2imgur.asm.implementation.MappingAdapter;
import net.labymod.core.asm.LabyModCoreMod;
import net.labymod.main.Source;
import net.labymod.utils.Debug;
import net.labymod.utils.Debug.EnumDebugMode;
import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class Screen2ImgurTransformer implements IClassTransformer {
	
	private static final String APPENDER_CLASS_NAME = "de/summerfeeling/screen2imgur/asm/PublishAppender";
	private static final String APPENDER_METHOD_NAME = "appendPublisher";
	
	private MappingAdapter implementation;
	
	public Screen2ImgurTransformer() {
		if (Source.ABOUT_MC_VERSION.startsWith("1.8")) {
			this.implementation = new MC18Implementation(LabyModCoreMod.isForge());
		} else if (Source.ABOUT_MC_VERSION.startsWith("1.12")) {
			this.implementation = new MC112Implementation(LabyModCoreMod.isForge());
		} else {
			System.out.println("[Screen2Imgur] This addon does not support your minecraft version " + Source.ABOUT_MC_VERSION + "!");
		}
		
		if (implementation != null) Debug.log(EnumDebugMode.ASM, "[Screen2Imgur] Using " + implementation.toString() + " mapping implementation");
	}
	
	public byte[] transform(String name, String transformedName, byte[] basicClass) {
		if (implementation == null) return basicClass;
		
		if (transformedName.equals(implementation.getHelperClassName())) {
			ClassNode classNode = new ClassNode();
			ClassReader classReader = new ClassReader(basicClass);
			classReader.accept(classNode, 0);
			
			methodFor : for (MethodNode methodNode : classNode.methods) {
				if (methodNode.name.equals(implementation.getSaveMethodName())) {
					if (methodNode.desc.equals(implementation.getSaveMethodDesc())) {
						Debug.log(EnumDebugMode.ASM, "[Screen2Imgur] Transforming " + methodNode.name + " @ " + transformedName + " (" + methodNode.desc + ")");
						
						for (int i = 0; i < methodNode.instructions.size(); i++) {
							AbstractInsnNode node = methodNode.instructions.get(i);
							
							if (node.getOpcode() == Opcodes.ARETURN) {
								AbstractInsnNode appenderHook = new MethodInsnNode(Opcodes.INVOKESTATIC, APPENDER_CLASS_NAME, APPENDER_METHOD_NAME, implementation.getAppenderMethodDesc(), false);
								methodNode.instructions.insertBefore(node, appenderHook);
								
								break methodFor;
							}
						}
					}
				}
			}
			
			ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);
			classNode.accept(classWriter);
			
			return classWriter.toByteArray();
		}
		
		return basicClass;
	}
}
