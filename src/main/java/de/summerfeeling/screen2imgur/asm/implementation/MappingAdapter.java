package de.summerfeeling.screen2imgur.asm.implementation;

public abstract class MappingAdapter {
	
	private boolean forgeClient;
	
	public MappingAdapter(boolean forgeClient) {
		this.forgeClient = forgeClient;
	}
	
	public String getHelperClassName() {
		if (forgeClient) return getForgeHelperClassName();
		return getObfuscatedHelperClassName();
	}
	
	public boolean isForgeClient() {
		return forgeClient;
	}
	
	public abstract String getObfuscatedHelperClassName();
	public abstract String getForgeHelperClassName();
	
	public abstract String getSaveMethodName();
	
	public abstract String getAppenderMethodDesc();
	public abstract String getSaveMethodDesc();
	
}
