package de.summerfeeling.screen2imgur.asm.implementation;

public class MC112Implementation extends MappingAdapter {
	
	public MC112Implementation(boolean forgeClient) {
		super(forgeClient);
	}
	
	@Override
	public String getObfuscatedHelperClassName() {
		return "avj";
	}
	
	@Override
	public String getForgeHelperClassName() {
		return "net.minecraft.util.ScreenShotHelper";
	}
	
	@Override
	public String getSaveMethodName() {
		return "a";
	}
	
	@Override
	public String getAppenderMethodDesc() {
		return "(Leu;)Leu;";
	}
	
	@Override
	public String getSaveMethodDesc() {
		return "(Ljava/io/File;Ljava/lang/String;IILbfw;)Leu;";
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "{" +
				"forgeClient=" + isForgeClient() +
				",helperClass=" + getHelperClassName() +
				",saveMethodName=" + getSaveMethodName() +
				",saveMethodDesc=" + getSaveMethodDesc() +
				",appenderMethoDesc=" + getAppenderMethodDesc() +
				'}';
	}
}
