package de.summerfeeling.screen2imgur.asm.implementation;

public class UnobfuscatedImplementation extends MappingAdapter {
	
	public UnobfuscatedImplementation(boolean forgeClient) {
		super(forgeClient);
	}
	
	@Override
	public String getObfuscatedHelperClassName() {
		return "net.minecraft.util.ScreenShotHelper";
	}
	
	@Override
	public String getForgeHelperClassName() {
		return "net.minecraft.util.ScreenShotHelper";
	}
	
	@Override
	public String getSaveMethodName() {
		return "saveScreenshot";
	}
	
	@Override
	public String getSaveMethodDesc() {
		return "(Ljava/io/File;Ljava/lang/String;IILnet/minecraft/client/shader/Framebuffer;)Lnet/minecraft/util/IChatComponent;";
	}
	
	@Override
	public String getAppenderMethodDesc() {
		return "(Lnet/minecraft/util/IChatComponent;)Lnet/minecraft/util/IChatComponent;";
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "{" +
				"forgeClient=" + isForgeClient() +
				",helperClass=" + getHelperClassName() +
				",saveMethodName=" + getSaveMethodName() +
				",saveMethodDesc=" + getSaveMethodDesc() +
				",appenderMethoDesc=" + getAppenderMethodDesc() +
				'}';
	}
}
